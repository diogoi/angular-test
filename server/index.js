const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
var path=require('path');

const app = express();
const router = express.Router(); 



app.use('/',express.static(path.join(__dirname, 'dist/web-app')));





app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

const data = [];


router.get('/data', (req, res) => {
  return res.json({ data });
});

router.post('/add', (req, res) => {
  console.log(req.body)
  const result = req.body;

  data.push(result);
  return res.json({ result });
});

app.use('/api',router);



app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/dist/web-app/index.html'))
})

app.listen(9000, () => console.log('listening'));

