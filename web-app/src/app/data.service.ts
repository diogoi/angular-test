import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  private dataURL = 'http://localhost:9000/api';

  constructor( private http: HttpClient) { }

  postData(data): Observable<any> {
    return this.http.post('/api/add', data);
  }

  getData(): Observable<any>  {
    return this.http.get('/api/data');
  }
}
