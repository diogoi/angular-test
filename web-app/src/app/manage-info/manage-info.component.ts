import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-manage-info',
  templateUrl: './manage-info.component.html',
  styleUrls: ['./manage-info.component.css']
})
export class ManageInfoComponent implements OnInit {

  infoData = [];

  constructor(private dataService: DataService) { }

  ngOnInit() {
  }

  onClickSubmit(formData) {
    this.dataService.postData(formData).subscribe();
  }

  getData(){
    this.dataService.getData().subscribe(response =>{
      this.infoData = response.data;
    })
  }

}
